/*
Copyright (C) 2011 James Coliz, Jr. <maniacbug@ymail.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.
*/
#ifndef MCP23018_BITBANG_H
#define MCP23018_BITBANG_H

#include "Arduino.h"
#include "..\BitBangI2C\BitBangI2C.h"

// Configuration Register
#define IOCON (0x05)

//Direction Registers
#define IODIRA (0x00)
#define IODIRB (0x10)

// General Purpose I/O Registers
#define GPIOA (0x09)
#define GPIOB (0x19)

// Output Latch Registers
#define OLATA (0x0a)
#define OLATB (0x1a)

// Pull-up Resistor Registers
#define GPPUA (0x06)
#define GPPUB (0x16)

class MCP23018_BitBang
{
protected:

	uint8_t i2c_address;

	void writeToRegister(uint8_t address, uint8_t data);
	uint8_t readFromRegister(uint8_t address);

	BitBangI2C i2c;

public:
	MCP23018_BitBang(uint8_t _address);

	void begin(void);

	void SetIODIRA(uint8_t _a);
	void SetIODIRB(uint8_t _b);

	uint8_t GetIODIRA(void);
	uint8_t GetIODIRB(void);

	void SetPullupsA(uint8_t _a);
	void SetPullupsB(uint8_t _b);

	uint8_t GetPullupsA(void);
	uint8_t GetPullupsB(void);

	void SetPortA(uint8_t _data);
	void SetPortB(uint8_t _data);

	uint8_t GetPortA(void);
	uint8_t GetPortB(void);

	void SetLatchPortA(uint8_t dataByte);
	void SetLatchPortB(uint8_t dataByte);

	uint8_t GetLatchPortA(void);
	uint8_t GetLatchPortB(void);
};

#endif