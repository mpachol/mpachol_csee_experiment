#ifndef MCDC04_H_
#define MCDC04_H_

#include "Arduino.h"
#include "..\BitBangI2C\BitBangI2C.h"
#include <Wire.h>

//Addresses
#define MCDC04_1_WRITE            (0xE8)
#define MCDC04_1_READ             (0xE9)

#define MCDC04_2_WRITE            (0xEA)
#define MCDC04_2_READ             (0xEB)

#define MCDC04_3_WRITE            (0xEC)
#define MCDC04_3_READ             (0xED)

#define MCDC04_4_WRITE            (0xEE)
#define MCDC04_4_READ             (0xEF)

//Registers
#define MCDC04_OSR                (0x00) //Operational State Register
#define MCDC04_AGEN				  (0x02) //API Generation
#define MCDC04_CREGL		      (0x06) //Configuration Register
#define MCDC04_CREGH 			  (0x07) //Configuration Register
#define MCDC04_OPTREG   	      (0x08) //Options Register
#define MDCD04_BREAK              (0x09) //Break Register
#define MDCD04_EDGES 			  (0x0A) //Edges Register

//Output
#define MCDC04_OUT0				  (0x00)
#define MCDC04_OUT1               (0x01)
#define MCDC04_OUT2               (0x02)
#define MCDC04_OUT3               (0x03)
#define MCDC04_OUTINT             (0x04)


class MCDC04 
{
 protected:

	uint8_t  i2c_address;
	uint8_t _pin ;
	void     writeToRegister(uint8_t address, uint8_t data);
	void     writeToRegister16(uint8_t address, uint16_t data);
	//uint8_t	 readFromRegister(uint8_t address) ;
	
	BitBangI2C i2c;
	
 public:

	MCDC04(uint8_t _address, uint8_t pin);
	void     begin(void);
    
	void  readFromRegister(uint8_t *CREGL, uint8_t *CREGH, uint8_t *OPTREG, uint8_t *BREAK, uint8_t *EDGES);
	void     SetCREGL(uint8_t _L); //CREGL
	void     SetCREGH(uint8_t _H); //CREGH
	uint8_t  GetCREGL(void); //CREGL
	uint8_t  GetCREGH(void); //CREGH
	
	void     SetState(uint8_t _M); //Operational State
	uint8_t  GetState(void); //Operational State
	
	void     SetOPT(uint8_t _O); //Options Registers
	uint8_t  GetOPT(void);
	
	void	 SetEdges(uint8_t _E); //Setting Edges
	void	 SetBreak(uint8_t _B); //Setting Break
	
	void     GetData(uint16_t *X, uint16_t *Y, uint16_t *Z);
	void  	clearRegisters(void);
	
	uint16_t GetX(void);
	uint16_t GetY(void);
	uint16_t GetZ(void);
};

#endif