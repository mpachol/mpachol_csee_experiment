//CubeSat Main Sketch on Slave Arduino

#include <SPI.h>
#include <Wire.h>

#include "BitBangI2C.h"
#include "MCP23018_BitBang.h"
#include "Adafruit_INA219_BitBang.h"
#include "EEPROMex.h"
#include "EEPROMVar.h"
#include "CubeSat_CSEE.h"
#include "MCDC04.h"

#define CSEE_I2C_ADDR 0x08

#define DO_NOTHING (0)
#define GET_DATA_SIZE_COMMAND (1)
#define GET_VOLTAGE_RESOLUTION_COMMAND (2)
#define SET_VOLTAGE_RESOLUTION_COMMAND (3)
#define GET_CURRENT_LED_COMMAND (4)
#define GET_COMPONENT_STATUS_COMMAND (5)
#define GET_LED_STATUS_COMMAND (6)
#define CHECK_COMPONENT_STATUS_COMMAND (7)
#define CHECK_LED_STATUS_COMMAND (8)
#define GET_LED_ENABLED_COMMAND (9)
#define ENABLE_LED_COMMAND (10)
#define DISABLE_LED_COMMAND (11)
#define RUN_EXPERIMENT_1_COMMAND (12)
#define RETRIEVE_DATA_COMMAND (13)
#define RUN_EXPERIMENT_2_COMMAND (14)
#define ENABLE_ARRAY_COMMAND (15)
#define DISABLE_ARRAY_COMMAND (16)
#define TEST_EEPROM (17)
#define TEST_RAM (18)
#define RETRIEVE_SAVED_DATA_COMMAND (19)

uint8_t CURRENT_COMMAND = DO_NOTHING;
uint8_t* DATA_RECEIVED = new uint8_t[3];
uint8_t RETURN_SIZE;
uint8_t RETURN_BYTES[32];
bool RETURN_READY = false;

uint8_t* ledStatusArray = new uint8_t[3];
uint8_t* ledEnabledArray = new uint8_t[3];

uint8_t retrievedData[32];
uint16_t retrievedDataCounter = 0;
uint16_t dataSize = 0x0802;

CubeSat_CSEE cubeSat = CubeSat_CSEE();
void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  cubeSat.Begin();
  
  Wire.begin(CSEE_I2C_ADDR);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
}

void loop() 
{
  // put your main code here, to run repeatedly:
  i2cReceivedFunction();
  delay(100);

}

// Grab I2C Command and any data bytes on write
void receiveEvent(int num_bytes)
{
    CURRENT_COMMAND = Wire.read();
    if(Wire.available())
    {
        DATA_RECEIVED[0] = Wire.read();
    }

}

// Write the requested data on I2C 
void requestEvent()
{

    if (RETURN_READY != false && RETURN_SIZE > 0) {
        for (int i = 0; i < RETURN_SIZE; i++)
            Wire.write(RETURN_BYTES[i]);
    }
}

void i2cReceivedFunction() {
    
    uint8_t* LEDStatusArray = new uint8_t[3];
    switch(CURRENT_COMMAND)
    {
        // These functions have no returns
        case SET_VOLTAGE_RESOLUTION_COMMAND:
            cubeSat.SetVoltageResolution(DATA_RECEIVED[0]);
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case CHECK_COMPONENT_STATUS_COMMAND:
            cubeSat.CheckComponentStatus();
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case CHECK_LED_STATUS_COMMAND:
            cubeSat.CheckLEDStatus();
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case ENABLE_LED_COMMAND:
            cubeSat.EnableLED(DATA_RECEIVED[0]);
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case DISABLE_LED_COMMAND:
            cubeSat.DisableLED(DATA_RECEIVED[0]);
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case ENABLE_ARRAY_COMMAND:
            cubeSat.EnableArray(DATA_RECEIVED[0]);
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case DISABLE_ARRAY_COMMAND:
            cubeSat.DisableArray(DATA_RECEIVED[0]);
            RETURN_SIZE = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        // These Functions return data    
        case GET_DATA_SIZE_COMMAND: //returns 2 bytes
            
            RETURN_READY = false;
            dataSize = cubeSat.GetDataSize();
            RETURN_BYTES[0] = dataSize >> 8;
            RETURN_BYTES[1] =  (dataSize << 8) >> 8;
            RETURN_SIZE = 2;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;
        
        case GET_VOLTAGE_RESOLUTION_COMMAND: //returns 1
            RETURN_READY = false;
            RETURN_BYTES[0] = cubeSat.GetVoltageResolution();
            RETURN_SIZE = 1;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;
        
        case GET_CURRENT_LED_COMMAND: //returns 1
            RETURN_READY = false;
            RETURN_BYTES[0] = cubeSat.GetCurrentLED();
            RETURN_SIZE = 1;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;
        
        case GET_COMPONENT_STATUS_COMMAND: //returns 1
            RETURN_READY = false;
            RETURN_BYTES[0] = cubeSat.GetComponentStatus();
            RETURN_SIZE = 1;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case GET_LED_STATUS_COMMAND: //returns 3
            RETURN_READY = false;
            cubeSat.GetLEDStatus(LEDStatusArray);
            RETURN_BYTES[0] = LEDStatusArray[0];
            RETURN_BYTES[1] = LEDStatusArray[1];
            RETURN_BYTES[2] = LEDStatusArray[2];
            RETURN_SIZE = 3;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case GET_LED_ENABLED_COMMAND: //returns 1
            RETURN_READY = false;
            cubeSat.GetEnabledLEDs(LEDStatusArray);
            RETURN_BYTES[0] = LEDStatusArray[0];
            RETURN_BYTES[1] = LEDStatusArray[1];
            RETURN_BYTES[2] = LEDStatusArray[2];
            RETURN_SIZE = 3;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case RUN_EXPERIMENT_1_COMMAND: //returns 2
            RETURN_READY = false;
            dataSize = cubeSat.RunExperiment1();
            RETURN_BYTES[0] = dataSize >> 8;
            RETURN_BYTES[1] = (dataSize << 8) >> 8;
            RETURN_SIZE = 2;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case RUN_EXPERIMENT_2_COMMAND: //returns 2
            RETURN_READY = false;
            dataSize = cubeSat.RunExperiment2();
            RETURN_BYTES[0] = dataSize >> 8;
            RETURN_BYTES[1] = (dataSize << 8) >> 8;
            RETURN_SIZE = 2;
            RETURN_READY = true;
            CURRENT_COMMAND = DO_NOTHING;
            break;

        case RETRIEVE_DATA_COMMAND: // returns 32 at a time, called 64 times
            RETURN_READY = false;
            cubeSat.RetrieveData(retrievedData, retrievedDataCounter);
            retrievedDataCounter++;
            // Get one packet of 32 bytes ready 
            for (int id = 0; id < 32; id ++) {
                RETURN_BYTES[id] = retrievedData[id];
            }
            RETURN_SIZE = 32;
            RETURN_READY = true;
            if (retrievedDataCounter == 64) {
                retrievedDataCounter = 0;
                CURRENT_COMMAND = DO_NOTHING;
            }
            break;
        case TEST_EEPROM:
            cubeSat.TestEEPROM();
            CURRENT_COMMAND = DO_NOTHING;
            break;
        case TEST_RAM:
            cubeSat.TestRAM();
            retrievedDataCounter = 0;
            CURRENT_COMMAND = DO_NOTHING;
            break;
        case RETRIEVE_SAVED_DATA_COMMAND: // returns 32 at a time, called 64 times
            RETURN_READY = false;
            cubeSat.RetrieveSavedData(retrievedData, retrievedDataCounter);
            retrievedDataCounter++;
            for (int id = 0; id < 32; id ++) {
                RETURN_BYTES[id] = retrievedData[id];
            }
            RETURN_SIZE = 32;
            RETURN_READY = true;
            if (retrievedDataCounter == 64) {
                retrievedDataCounter = 0;
                CURRENT_COMMAND = DO_NOTHING;
            }
            break;
        default:
            //Do nothing
            break;
    }
}
