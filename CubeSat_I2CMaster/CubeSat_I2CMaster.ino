#include <Wire.h>

#define BUFFER_LENGTH (E2END)

#define GET_DATA_SIZE_COMMAND (1)
#define GET_VOLTAGE_RESOLUTION_COMMAND (2)
#define SET_VOLTAGE_RESOLUTION_COMMAND (3)
#define GET_CURRENT_LED_COMMAND (4)
#define GET_COMPONENT_STATUS_COMMAND (5)
#define GET_LED_STATUS_COMMAND (6)
#define CHECK_COMPONENT_STATUS_COMMAND (7)
#define CHECK_LED_STATUS_COMMAND (8)
#define GET_LED_ENABLED_COMMAND (9)
#define ENABLE_LED_COMMAND (10)
#define DISABLE_LED_COMMAND (11)
#define RUN_EXPERIMENT_1_COMMAND (12)
#define RETRIEVE_DATA_COMMAND (13)

uint8_t* LEDStatusArray = new uint8_t[3];
uint8_t* enabledLEDsArray = new uint8_t[3];

uint16_t dataSize;
uint8_t voltageResolution;

void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  Wire.begin();
  Wire.setClock(50000L);
}

char serialCharacter = 0;
String serialCommandInput = "";

void loop() 
{
  // put your main code here, to run repeatedly:
  if(Serial.available() > 0)
  {
    serialCharacter = Serial.read();

    if(serialCharacter != '\n')
    {
      serialCommandInput += serialCharacter;
    }
    else
    {
      ProcessCommand(serialCommandInput);
      serialCommandInput = "";
    }
  }
}

void ProcessCommand(String serialCommandInput)
{
  if(serialCommandInput.equals("GETDATASIZE"))
  {
    Serial.print("Data Packet Size: ");
    Serial.println(GetDataSize());
  }
  else if(serialCommandInput.equals("GETVOLTAGERESOLUTION"))
  {
    Serial.print("Voltage Resolution: ");
    Serial.println(GetVoltageResolution());
  }
  else if(serialCommandInput.equals("GETCURRENTLED"))
  {
    Serial.print("Current LED: ");
    Serial.println(GetCurrentLED());
  }
  else if(serialCommandInput.equals("GETCOMPONENTSTATUS"))
  {
    Serial.print("Component Status: ");
    Serial.println(GetComponentStatus(), BIN);
  }
  else if(serialCommandInput.equals("GETLEDSTATUS"))
  {
    GetLEDStatus(LEDStatusArray);
    Serial.println("Initial LED Status: ");
    Serial.print("Array 1: ");
    Serial.println(LEDStatusArray[0], BIN);
    Serial.print("Array 2: ");
    Serial.println(LEDStatusArray[1], BIN);
    Serial.print("Array 3: ");
    Serial.println(LEDStatusArray[2], BIN);
  }
  else if(serialCommandInput.equals("GETENABLEDLEDS"))
  {
    GetEnabledLEDs(enabledLEDsArray);
    Serial.println("Enabled LEDs: ");
    Serial.print("Array 1: ");
    Serial.println(enabledLEDsArray[0], BIN);
    Serial.print("Array 2: ");
    Serial.println(enabledLEDsArray[1], BIN);
    Serial.print("Array 3: ");
    Serial.println(enabledLEDsArray[2], BIN);
  }
  else if(serialCommandInput.equals("RUNEXPERIMENT1"))
  {
    Serial.println("Running Experiment 1...");
    dataSize = RunExperiment1();
    Serial.print(dataSize);
    Serial.println(" bytes of data gathered. Ready to Retrieve");
  }
  else if(serialCommandInput.equals("RETRIEVEDATA"))
  {
    uint8_t* experiment1DataArray = new uint8_t[dataSize];
    voltageResolution = RetrieveData(experiment1DataArray);
    PrintRetrievedData(experiment1DataArray);
  }

  // All the different command codes to set voltage resolution and enable/disable LEDs
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION8"))
  {
    SetVoltageResolution(8);
    Serial.println("Voltage Resolution Set To 8");
  }
  else if(serialCommandInput.equals("CHECKCOMPONENTSTATUS"))
  {
    CheckComponentStatus();
    Serial.println("Checking status of components");
  }
  else if(serialCommandInput.equals("CHECKLEDSTATUS"))
  {
    CheckLEDStatus();
    Serial.println("Checking status of LEDs");
  }
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION16"))
  {
    SetVoltageResolution(16);
    Serial.println("Voltage Resolution Set To 16");
  }
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION32"))
  {
    SetVoltageResolution(32);
    Serial.println("Voltage Resolution Set To 32");
  }
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION64"))
  {
    SetVoltageResolution(64);
    Serial.println("Voltage Resolution Set To 64");
  }
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION128"))
  {
    SetVoltageResolution(128);
    Serial.println("Voltage Resolution Set To 128");
  }
  else if(serialCommandInput.equals("SETVOLTAGERESOLUTION256"))
  {
    SetVoltageResolution(256);
    Serial.println("Voltage Resolution Set To 256");
  }
  else if(serialCommandInput.equals("ENABLELED1"))
  {
    EnableLED(1);
    Serial.println("LED 1 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED1"))
  {
    DisableLED(1);
    Serial.println("LED 1 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED2"))
  {
    EnableLED(2);
    Serial.println("LED 2 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED2"))
  {
    DisableLED(2);
    Serial.println("LED 2 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED3"))
  {
    EnableLED(3);
    Serial.println("LED 3 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED3"))
  {
    DisableLED(3);
    Serial.println("LED 3 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED4"))
  {
    EnableLED(4);
    Serial.println("LED 4 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED4"))
  {
    DisableLED(4);
    Serial.println("LED 4 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED5"))
  {
    EnableLED(5);
    Serial.println("LED 5 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED5"))
  {
    DisableLED(5);
    Serial.println("LED 5 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED6"))
  {
    EnableLED(6);
    Serial.println("LED 6 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED6"))
  {
    DisableLED(6);
    Serial.println("LED 6 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED7"))
  {
    EnableLED(7);
    Serial.println("LED 7 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED7"))
  {
    DisableLED(7);
    Serial.println("LED 7 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED8"))
  {
    EnableLED(8);
    Serial.println("LED 8 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED8"))
  {
    DisableLED(8);
    Serial.println("LED 8 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED9"))
  {
    EnableLED(9);
    Serial.println("LED 9 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED9"))
  {
    DisableLED(9);
    Serial.println("LED 9 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED10"))
  {
    EnableLED(10);
    Serial.println("LED 10 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED10"))
  {
    DisableLED(10);
    Serial.println("LED 10 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED11"))
  {
    EnableLED(11);
    Serial.println("LED 11 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED11"))
  {
    DisableLED(11);
    Serial.println("LED 11 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED12"))
  {
    EnableLED(12);
    Serial.println("LED 12 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED12"))
  {
    DisableLED(12);
    Serial.println("LED 12 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED13"))
  {
    EnableLED(13);
    Serial.println("LED 13 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED13"))
  {
    DisableLED(13);
    Serial.println("LED 13 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED14"))
  {
    EnableLED(14);
    Serial.println("LED 14 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED14"))
  {
    DisableLED(14);
    Serial.println("LED 14 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED15"))
  {
    EnableLED(15);
    Serial.println("LED 15 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED15"))
  {
    DisableLED(15);
    Serial.println("LED 15 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED16"))
  {
    EnableLED(16);
    Serial.println("LED 16 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED16"))
  {
    DisableLED(16);
    Serial.println("LED 16 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED17"))
  {
    EnableLED(17);
    Serial.println("LED 17 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED17"))
  {
    DisableLED(17);
    Serial.println("LED 17 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED18"))
  {
    EnableLED(18);
    Serial.println("LED 18 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED18"))
  {
    DisableLED(18);
    Serial.println("LED 18 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED19"))
  {
    EnableLED(19);
    Serial.println("LED 19 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED19"))
  {
    DisableLED(19);
    Serial.println("LED 19 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED20"))
  {
    EnableLED(20);
    Serial.println("LED 20 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED20"))
  {
    DisableLED(20);
    Serial.println("LED 20 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED21"))
  {
    EnableLED(21);
    Serial.println("LED 21 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED21"))
  {
    DisableLED(21);
    Serial.println("LED 21 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED22"))
  {
    EnableLED(22);
    Serial.println("LED 22 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED22"))
  {
    DisableLED(22);
    Serial.println("LED 22 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED23"))
  {
    EnableLED(23);
    Serial.println("LED 23 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED23"))
  {
    DisableLED(23);
    Serial.println("LED 23 Disabled");
  }
   else if(serialCommandInput.equals("ENABLELED24"))
  {
    EnableLED(24);
    Serial.println("LED 24 Enabled");
  }
  else if(serialCommandInput.equals("DISABLELED24"))
  {
    DisableLED(24);
    Serial.println("LED 24 Disabled");
  }
  else
  {
    Serial.println("Error: Command Code Not Recognized.");
  }
}

uint16_t GetDataSize()
{
  uint16_t dataSize = 0xFFFF;
  
  Wire.beginTransmission(8);
  Wire.write(GET_DATA_SIZE_COMMAND);
  Wire.endTransmission();

  Wire.requestFrom(8, 2);
  
  if(Wire.available() > 1)
  {
    dataSize = (Wire.read() << 8) | Wire.read();
  }
  
  return dataSize;
}

uint8_t GetVoltageResolution()
{
  uint8_t voltageResolution = 0xFF;
  
  Wire.beginTransmission(8);
  Wire.write(GET_VOLTAGE_RESOLUTION_COMMAND);
  Wire.endTransmission();
  
  Wire.requestFrom(8, 1);
  if(Wire.available())
  {
    voltageResolution = Wire.read();
  }
  
  return voltageResolution;
}

void SetVoltageResolution(uint8_t voltageResolution)
{
  Wire.beginTransmission(8);
  Wire.write(SET_VOLTAGE_RESOLUTION_COMMAND);
  Wire.write(voltageResolution);
  Wire.endTransmission();
}

uint8_t GetCurrentLED()
{
  uint8_t currentLED = 0xFF;
  
  Wire.beginTransmission(8);
  Wire.write(GET_CURRENT_LED_COMMAND);
  Wire.endTransmission();
  
  Wire.requestFrom(8, 1);
  if(Wire.available())
  {
    currentLED = Wire.read();
  }
  
  return currentLED;
}

void CheckComponentStatus()
{
  Wire.beginTransmission(8);
  Wire.write(CHECK_COMPONENT_STATUS_COMMAND);
  Wire.endTransmission();
}

uint8_t GetComponentStatus()
{
  uint8_t componentStatus = 0x00;
  
  Wire.beginTransmission(8);
  Wire.write(GET_COMPONENT_STATUS_COMMAND);
  Wire.endTransmission();
  
  Wire.requestFrom(8, 1);
  if(Wire.available())
  {
    componentStatus = Wire.read();
  }
  
  return componentStatus;
}

void CheckLEDStatus()
{
  Wire.beginTransmission(8);
  Wire.write(CHECK_LED_STATUS_COMMAND);
  Wire.endTransmission();
}

void GetLEDStatus(uint8_t* LEDStatusArray)
{
  Wire.beginTransmission(8);
  Wire.write(GET_LED_STATUS_COMMAND);
  Wire.endTransmission();
  
  Wire.requestFrom(8, 3);
  for(int i = 0; i < 3; i++)
  {
    if(Wire.available())
    {
      LEDStatusArray[i] = Wire.read();
    }
  }
}

void GetEnabledLEDs(uint8_t* enabledLEDsArray)
{
  Wire.beginTransmission(8);
  Wire.write(GET_LED_ENABLED_COMMAND);
  Wire.endTransmission();
  
  Wire.requestFrom(8, 3);
  for(int i = 0; i < 3; i++)
  {
    if(Wire.available())
    {
      enabledLEDsArray[i] = Wire.read();
    }
  }
}

void EnableLED(uint8_t LEDToEnable)
{
  if(LEDToEnable < 25)
  {
    Wire.beginTransmission(8);
    Wire.write(ENABLE_LED_COMMAND);
    Wire.write(LEDToEnable);
    Wire.endTransmission();
  }
}

void DisableLED(uint8_t LEDToDisable)
{
  if(LEDToDisable < 25)
  {
    Wire.beginTransmission(8);
    Wire.write(DISABLE_LED_COMMAND);
    Wire.write(LEDToDisable);
    Wire.endTransmission();
  }
}

uint16_t RunExperiment1()
{
  uint16_t dataSize = 0xFFFF;
  
  Wire.beginTransmission(8);
  Wire.write(RUN_EXPERIMENT_1_COMMAND);
  Wire.endTransmission();

  Wire.requestFrom(8, 2);
  
  while(Wire.available() < 2)
  {
    delay(50);
  }
  dataSize = (Wire.read() << 8) | Wire.read();
  
  return dataSize;
}

uint8_t RetrieveData(uint8_t* retrievedDataArray)
{
  Wire.beginTransmission(8);
  Wire.write(RETRIEVE_DATA_COMMAND);
  Wire.endTransmission();
  uint8_t readData;
  for(int i = 0; i < dataSize; i++)
  {
    Wire.requestFrom(8, 1);
    if(Wire.available())
    {
      retrievedDataArray[i] = Wire.read();
    }
  }
  
  return GetVoltageResolution();
}

void PrintRetrievedData(uint8_t* dataArray)
{
  Serial.println();
  int arrayCounter = 0;
  while(arrayCounter < dataSize)
  {
    Serial.print("LED: ");
    Serial.println(dataArray[arrayCounter]);
    arrayCounter++;

    Serial.print("Time Stamp: ");
    Serial.println((dataArray[arrayCounter] << 8) | (dataArray[arrayCounter + 1]));
    Serial.println();
    arrayCounter++;
    arrayCounter++;
    for(int i = 0; i < voltageResolution; i++)
    {
      Serial.print("Voltage: ");
      float voltage = (dataArray[arrayCounter] | dataArray[arrayCounter + 1] << 8);
      voltage /= 100;
      Serial.print(voltage);
      arrayCounter++;
      arrayCounter++;

      Serial.print(", Current: ");
      float current = (dataArray[arrayCounter] | dataArray[arrayCounter + 1] << 8);
      current /= 100;
      Serial.println(current);
      arrayCounter++;
      arrayCounter++;
    }

    Serial.println();
  }
}
